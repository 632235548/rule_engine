@auth:weibi
@date:2020-03-19
一、引擎的由来
目前我已对接过华西医院、福清医院，近期在对接梧州人民医院，发现代码重复的工作量很大，为降低开发时间，缩短项目交付时间，于是有感而发，自主开发了这个引擎。

二、引擎的好处：
1、降低开发时间，缩短项目交付时间
2、灵活的数据库配置
3、不需要维护各家医院的代码（多），只维护该引擎即可，降低维护成本
4、使用completablefuture异步编程，缩短接口响应时间，有效提高用户体验

三、引擎的规则目前支持：(目前院方接口基本都是xml、json格式)
   1、入参格式目前支持 json 、xml，入参参数支持java、js函数，
   2、支持json、xml 多子孙节点入参情况
   3、在高并发情况下，我做了多线程处理，达到非阻塞目的，有效提高接口反应时间
   4、在测试简阳医院的时候，有接口合并的操作，所以也支持了接口合并的操作处理

四、入参规则：
     以梧州人民医院为例：
     获取科室列表：
        我们只需要在数据库配置如下：
       <Request>
            <commandCode>GetDepartment</commandCode>
           <userId>讯飞</userId>
           <timeStamp><#>java#com.iflytek.test.service.Utils#currentDateTime#yyyy-MM-dd HH:mm:ss</#></timeStamp>
           <password><#>java#com.iflytek.test.service.Utils#createSecret#{timeStamp}#XUNFEIambition-sky</#></password>
           <OnlyRemain>0</OnlyRemain>
           <SchDate><#>java#com.iflytek.test.service.Utils#currentDateTime#yyyy-MM-dd</#></SchDate>
           <SchDay>7</SchDay>
      </Request> 
   获取医生列表：
      <Request>
 <commandCode>GetDoctor</commandCode>
 <userId>讯飞</userId>
 <timeStamp><#>java#com.iflytek.test.service.Utils#currentDateTime#yyyy-MM-dd HH:mm:ss</#></timeStamp>
 <password><#>java#com.iflytek.test.service.Utils#createSecret#{timeStamp}#XUNFEIambition-sky</#></password>
 <AccessSectId><#>attr#deptId</#></AccessSectId>
 <SchDate><#>java#com.iflytek.test.service.Utils#currentDateTime#yyyy-MM-dd</#></SchDate>
 <SchDay><#>js#getSchDay#function getSchDay() { return "7"}</#></SchDay>
    </Request>

   说明：
       1）固定参数，不做任何处理，如 GetDepartment ，就直接 GetDepartment
       2）可变参数：如 timeStamp 节点，如果需要执行代码片段或者需要请求对象中的参数值 ，则必须以 <#> 开始</#>结束
       3）执行java 代码格式：<#>java#类路径#方法#参数#参数</#>   
                    注意：参数可有可无，具体看你的方法 ，如节点  SchDate
        4）执行 js 函数格式：<#>js#方法名#代码#参数#参数</#>
                  注意：参数可有可无，具体看你的方法 ,js函数必须有返回值，否则无意义，如获取医生列表节点 SchDay  
        5）如果入参节点的值需要上一个节点值，格式为 {} ，如 password 节点
        6）如果入参节点值需要入参对象的属性值，格式为 attr[属性] 
        7）如果入参节点值需要入参对象的属性值，则格式为  <#> attr#入参对象的属性</#> ， 如上述节点 AccessSectId 
         8） 目前不支持 参数嵌套代码，不支持js、java 相互嵌套

五、出参规则：
       如 ：梧州人民医院获取科室列表的出参配置

说明：
     1）returnCodeNode 字段：作用是接口返回code判断
     2）listNode 字段：数据节点
     3）mapping 字段：返回字段与院方接口放回的映射字段，格式：[]$[]  必须一一对应上，前面[]为我们的返回对象属性，后面为院方接口返回的对象属性
   注：1、某个出参需要相关出参的转换以得到最终结果，这时就需要和入参规则一样，可以用 java或者js函数，注意这里就不需要以<#></#>
       如梧州人民医院：通过日期获取排班信息的出参配置：
 [deptId,deptName,doctorId,docName,regDate,regWeek,arrangeId,regTimeHIS,regTimeName,regFee,regTotalCount,regTotalCount] $
 [AccessDeptId,DeptName,AccessDoctorId,DoctorName,SchDate,java#com.iflytek.test.service.Utils#dateToWeek{regDate},AccessSchId,DayType,java#com.iflytek.test.service.Utils#getDayType#{regTimeHIS},Cost,ResNo,Remain]
        2、出参值可能由有入参对象值提供，和入参规则一样，格式 attr#入参对象属性
   以上配置，将自动解析，不再需要人为写代码
