﻿# Host: 172.31.197.22:8002  (Version 5.7.21-21-log)
# Date: 2020-03-18 09:51:17
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "rule_merchant"
#

CREATE TABLE `rule_merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant` varchar(60) NOT NULL DEFAULT '',
  `serverUrl` varchar(255) NOT NULL DEFAULT '' COMMENT '请求服务的地址',
  `serverType` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型1webservice 2http或https',
  `serverNamespace` varchar(255) DEFAULT NULL COMMENT '命名空间',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createUser` varchar(128) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Structure for table "rule_request"
#

CREATE TABLE `rule_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant` varchar(60) NOT NULL DEFAULT '',
  `requestMethod` varchar(60) NOT NULL DEFAULT '' COMMENT '请求方法',
  `xfRequestMethod` varchar(60) NOT NULL DEFAULT '' COMMENT '讯飞请求方法',
  `requestParamCommon` varchar(1024) DEFAULT NULL COMMENT '请求公共部分',
  `requestParam` varchar(1024) NOT NULL DEFAULT '' COMMENT '请求参数',
  `requestParamType` tinyint(3) NOT NULL DEFAULT '2' COMMENT '请求参数类型1json2xml3字符串',
  `requestType` tinyint(3) NOT NULL DEFAULT '0' COMMENT '请求方式 0Soap1Get2Post',
  `requestDesc` varchar(255) DEFAULT NULL COMMENT '请求描述',
  `creatTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createUser` varchar(128) DEFAULT '' COMMENT '创建人',
  `rpcMethod` varchar(255) DEFAULT NULL COMMENT '格式: java#类路径#方法',
  `filter` varchar(255) NOT NULL DEFAULT '' COMMENT '过滤条件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

#
# Structure for table "rule_result"
#

CREATE TABLE `rule_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant` varchar(60) DEFAULT NULL,
  `returnCodeNode` varchar(60) DEFAULT NULL COMMENT '接口返回错误码节点',
  `listNode` varchar(60) DEFAULT NULL COMMENT '集合节点',
  `mapping` varchar(1024) DEFAULT NULL COMMENT '映射关系json',
  `xfRequestMethod` varchar(60) DEFAULT NULL COMMENT '讯飞接口方法',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createUser` varchar(128) DEFAULT '' COMMENT '创建人',
  `returnMsgNode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
