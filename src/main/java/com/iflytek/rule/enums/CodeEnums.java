package com.iflytek.rule.enums;

/**
 * @Author weibi
 * @Date 2020/3/3 8:26
 * @Version 1.0
 * @Description TODO
 **/
public enum CodeEnums {

    SUCCESS("1000", "操作成功"),
    NULL_OBJECT("1001", "对象不能空"),
    NULL_PARAM("1002", "参数不能空"),
    EXCEPTION_PARAM("1003", "参数异常"),

    RESULT_CODE_ERROR("1004", "接口返回code异常"),
    RESULT_DATA_ERROR("1005", "接口返回data异常");

    CodeEnums(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }}
