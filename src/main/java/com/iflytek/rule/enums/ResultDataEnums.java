package com.iflytek.rule.enums;

import cn.hutool.core.util.StrUtil;
import com.iflytek.rule.strategy.resultdata.CheckCardExistResultData;
import com.iflytek.rule.strategy.resultdata.CreatePatMedCardResultData;
import com.iflytek.rule.strategy.resultdata.GetAllRegOrderResultData;
import com.iflytek.rule.strategy.resultdata.GetDeptInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetDoctorInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetLocaleRegTimePointInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetOrderRegInfoforDiagnoseResultData;
import com.iflytek.rule.strategy.resultdata.GetOrderRegInfoforExpertResultData;
import com.iflytek.rule.strategy.resultdata.GetOrderRegTimePointInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetPatinfoReultData;
import com.iflytek.rule.strategy.resultdata.GetPrintRegOrderResultData;
import com.iflytek.rule.strategy.resultdata.GetSchedulingByDateResultData;
import com.iflytek.rule.strategy.resultdata.GetSchedulingByDepResultData;
import com.iflytek.rule.strategy.resultdata.GetSchedulingByDocResultData;
import com.iflytek.rule.strategy.resultdata.GetUpperDeptInfoResultData;
import com.iflytek.rule.strategy.resultdata.LocaleRegLockResultData;
import com.iflytek.rule.strategy.resultdata.OrderRegLockResultData;

public enum ResultDataEnums {

    GETUPPERDEPTINFO("getUpperDeptInfo", GetUpperDeptInfoResultData.class, "获取父级科室列表"),
    GETDEPTINFO("getDeptInfo", GetDeptInfoResultData.class, "获取子级科室列表"),
    GETDOCTINFO("getDoctorInfo", GetDoctorInfoResultData.class, "获取医生列表"),
    GETSCHEDULINGBYDATE("getOrderRegInfobyDate", GetSchedulingByDateResultData.class, "获取排班信息 by date"),
    GETSCHEDULINGBYDOC("getOrderRegInfobyDoc", GetSchedulingByDocResultData.class, "获取排班信息 by doc"),
    GETSCHEDULINGBYDEP("getOrderRegInfobyDep", GetSchedulingByDepResultData.class, "获取排班信息 by dep"),
    GETREMAINBYDOC("getOrderRegInfoforDiagnose", GetOrderRegInfoforDiagnoseResultData.class, "获取预检分诊号源结果"),
    GETREMAINBYDEPT("getOrderRegInfoforExpert", GetOrderRegInfoforExpertResultData.class, "挂号专家号源结果"),
    CREATEPATCARD("createPatMedCard", CreatePatMedCardResultData.class, "建档发卡"),
    GETPATINFO("getPatinfo", GetPatinfoReultData.class, "获取患者信息(读卡)"),
    PRINTGEGORDER("getPrintRegOrder", GetPrintRegOrderResultData.class, "挂号打印"),
    LOCALELOCK("localeRegLock", LocaleRegLockResultData.class, "当日挂号锁号"),
    ORDERREGLOCK("orderReg", OrderRegLockResultData.class, "预约挂号锁号"),
    CHECKCARDEXIST("checkCardExist", CheckCardExistResultData.class, "就诊卡存在校验"),
    GETORDERREGTIMEPOINTINFO("getOrderRegTimePointInfo", GetOrderRegTimePointInfoResultData.class, "医生可预约号源具体时间点查询"),
    GETLOCALEREGTIMEPOINTINFO("getLocaleRegTimePointInfo", GetLocaleRegTimePointInfoResultData.class, "当日挂号号源具体时间点查询"),
    GETALLREGORDER("getAllRegOrder", GetAllRegOrderResultData.class, "挂号记录查询");


    public static ResultDataEnums getResultDataEnums(String xfMethod) {

        if (StrUtil.isBlank(xfMethod)) {
            return null;
        }

        for (ResultDataEnums enums : ResultDataEnums.values()) {
            if (xfMethod.equals(enums.getXfMethod())) {
                return enums;
            }
        }
        return null;
    }


    ResultDataEnums(String xfMethod, Class clazz, String remark) {
        this.xfMethod = xfMethod;
        this.clazz = clazz;
        this.remark = remark;
    }

    private String xfMethod;
    private Class clazz;
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getXfMethod() {
        return xfMethod;
    }

    public void setXfMethod(String xfMethod) {
        this.xfMethod = xfMethod;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
