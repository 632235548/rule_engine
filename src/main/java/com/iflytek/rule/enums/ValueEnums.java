package com.iflytek.rule.enums;

import cn.hutool.core.util.StrUtil;
import com.iflytek.rule.strategy.valueengine.AttrValueEngine;
import com.iflytek.rule.strategy.valueengine.JavaValueEngine;
import com.iflytek.rule.strategy.valueengine.JsValueEngine;

/**
 * @Author weibi
 * @Date 2020/3/4 8:28
 * @Version 1.0
 * @Description TODO
 **/
public enum ValueEnums {

    VALUE_JAVA("java", JavaValueEngine.class),
    VALUE_JS("js", JsValueEngine.class),
    VALUE_ATTR("attr", AttrValueEngine.class);


    public static ValueEnums getValueEnums(String valueType) {

        if (StrUtil.isBlank(valueType)) {
            return null;
        }

        for (ValueEnums enums : ValueEnums.values()) {
            if (valueType.equals(enums.getValueType())) {
                return enums;
            }
        }
        return null;
    }


    ValueEnums(String valueType, Class clazz) {
        this.valueType = valueType;
        this.clazz = clazz;
    }

    private String valueType;
    private Class clazz;

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
