package com.iflytek.rule.enums;

import cn.hutool.core.util.StrUtil;
import com.iflytek.rule.strategy.resulttype.JSONResultType;
import com.iflytek.rule.strategy.resulttype.XMLResultType;

public enum ResultTypeEnums {


    JSON_RESULT_TYPE("json", JSONResultType.class),
    XML_RESULT_TYPE("xml", XMLResultType.class);


    public static ResultTypeEnums getResultTypeEnums(String resultType) {

        if (StrUtil.isBlank(resultType)) {
            return null;
        }

        for (ResultTypeEnums enums : ResultTypeEnums.values()) {
            if (resultType.equals(enums.getResultType())) {
                return enums;
            }
        }
        return null;
    }


    ResultTypeEnums(String resultType, Class clazz) {
        this.resultType = resultType;
        this.clazz = clazz;
    }

    private String resultType;
    private Class clazz;

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
