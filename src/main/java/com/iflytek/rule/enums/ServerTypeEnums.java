package com.iflytek.rule.enums;

import cn.hutool.core.util.StrUtil;
import com.iflytek.rule.strategy.servertype.HttpServerType;
import com.iflytek.rule.strategy.servertype.WebServiceServerType;

public enum ServerTypeEnums {


    WEBSERVICE_SERVER_TYPE(1, "webservice", WebServiceServerType.class),
    HTTP_SERVER_TYPE(2, "http", HttpServerType.class);


    public static ServerTypeEnums getServerTypeEnums(int Type) {

        for (ServerTypeEnums enums : ServerTypeEnums.values()) {
            if (Type == (enums.getType())) {
                return enums;
            }
        }
        return null;
    }


    ServerTypeEnums(int type, String serverType, Class clazz) {
        this.type = type;
        this.serverType = serverType;
        this.clazz = clazz;
    }

    private int type;
    private String serverType;
    private Class clazz;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }


}
