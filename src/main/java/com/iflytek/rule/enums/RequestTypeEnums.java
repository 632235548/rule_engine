package com.iflytek.rule.enums;

import cn.hutool.core.util.StrUtil;
import com.iflytek.rule.strategy.requestparamtype.JSONRequestType;
import com.iflytek.rule.strategy.requestparamtype.XMLRequestType;

public enum RequestTypeEnums {


    JSON_REQUEST_TYPE("json", JSONRequestType.class),
    XML_REQUEST_TYPE("xml", XMLRequestType.class);


    public static RequestTypeEnums getRequestTypeEnums(String requestType) {

        if (StrUtil.isBlank(requestType)) {
            return null;
        }

        for (RequestTypeEnums enums : RequestTypeEnums.values()) {
            if (requestType.equals(enums.getRequestType())) {
                return enums;
            }
        }
        return null;
    }


    RequestTypeEnums(String requestType, Class clazz) {
        this.requestType = requestType;
        this.clazz = clazz;
    }

    private String requestType;
    private Class clazz;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
