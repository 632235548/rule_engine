package com.iflytek.rule.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author weibi
 * @Date 2020/3/4 21:24
 * @Version 1.0
 * @Description TODO
 **/
@Data
public class RuleResultModel implements Serializable {

    private Long id;
    private String merchant;
    private String returnCodeNode;
    private String listNode;
    private String mapping;
    private String xfRequestMethod;
    private String returnMsgNode;

}
