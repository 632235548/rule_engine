package com.iflytek.rule.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author weibi
 * @Date 2020/3/2 17:18
 * @Version 1.0
 * @Description TODO
 **/
@Data
public class RuleRequestModel implements Serializable {

    private Long id;
    private String merchant;
    private String requestMethod;
    private String xfRequestMethod;
    private String requestParamCommon;
    private String requestParam;
    private Integer requestParamType;
    private Integer requestType;
    private String rpcMethod;
    private String requestDesc;
    private String filter;
    private Long pid;
    private Integer sort;
    private Integer isDeliver;

}
