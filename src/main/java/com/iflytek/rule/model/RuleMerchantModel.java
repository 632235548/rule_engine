package com.iflytek.rule.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author weibi
 * @Date 2020/3/3 11:02
 * @Version 1.0
 * @Description TODO
 **/
@Data
public class RuleMerchantModel implements Serializable {


    private Long id;
    private String merchant;
    private String serverUrl;
    private Integer serverType;
    private String serverNamespace;


}
