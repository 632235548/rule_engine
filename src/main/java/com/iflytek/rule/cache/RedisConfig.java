package com.iflytek.rule.cache;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/8 19:27
 * @Version 1.0
 * @Description TODO
 **/
@Data
@Component
@ConfigurationProperties(prefix = "spring.rule.redis")
public class RedisConfig {

    private int database;
    private Map<String, Long> timeout;

}
