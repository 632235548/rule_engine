package com.iflytek.rule.strategy.requestparamtype;

import cn.hutool.core.util.ObjectUtil;
import com.iflytek.rule.model.RuleRequestModel;
import com.iflytek.rule.strategy.ValueEngineStrategy;
import com.iflytek.rule.utils.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/3 9:17
 * @Version 1.0
 * @Description xml 格式参数
 **/
public class XMLRequestType implements IRequestType {

    @Override
    public Object operatorByRequestType(Object object,
                                        RuleRequestModel model) {
        try {

            String requestParam = model.getRequestParam();
            String tmpRequestParam = Utils.filter(requestParam, model.getFilter());
            Map<String, Object> map = Utils.getParamValue(tmpRequestParam, "xml");
            if (ObjectUtil.isNull(map)) {
                return requestParam;
            }
            Map<String, Object> tmpMap = Utils.paramToMap(map, object);
            // 填充值
            return Utils.mapToParam(requestParam, tmpMap, map);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
