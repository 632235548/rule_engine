package com.iflytek.rule.strategy.requestparamtype;

import com.iflytek.rule.model.RuleRequestModel;

/**
 * @Author weibi
 * @Date 2020/3/3 9:06
 * @Version 1.0
 * @Description TODO
 **/
public interface IRequestType {

    Object operatorByRequestType(Object obj, RuleRequestModel model);

}
