package com.iflytek.rule.strategy;

import cn.hutool.extra.spring.SpringUtil;
import com.iflytek.rule.enums.CodeEnums;
import com.iflytek.rule.enums.RequestTypeEnums;
import com.iflytek.rule.exceptions.BusinessException;
import com.iflytek.rule.model.RuleRequestModel;
import com.iflytek.rule.strategy.requestparamtype.IRequestType;
import org.springframework.context.annotation.Import;

@Import(cn.hutool.extra.spring.SpringUtil.class)
/**
 * @Author weibi
 * @Date 2020/3/3 9:01
 * @Version 1.0
 * @Description 请求参数类型模型
 **/
public class RequestTypeStrategy {

    public static RequestTypeStrategy getInstance() {
        return RequestTypeHandle.instance;
    }

    private RequestTypeStrategy() {
    }

    private static class RequestTypeHandle {
        private static RequestTypeStrategy instance = new RequestTypeStrategy();
    }

    public Object requestType(Object obj, RuleRequestModel model) {

        try {
            String code = getRequestType(model.getRequestParamType());
            RequestTypeEnums enums = RequestTypeEnums.getRequestTypeEnums(code);
            IRequestType iRequestType = (IRequestType) SpringUtil.getBean(enums.getClazz());
            return iRequestType.operatorByRequestType(obj, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getRequestType(Integer str) {
        switch (str) {
            case 1:
                return "json";
            case 2:
                return "xml";
            default:
                throw new BusinessException(CodeEnums.EXCEPTION_PARAM);
        }
    }

}
