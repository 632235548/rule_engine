package com.iflytek.rule.strategy;

import cn.hutool.extra.spring.SpringUtil;
import com.iflytek.rule.enums.ValueEnums;
import com.iflytek.rule.strategy.valueengine.IValueEngine;
import com.iflytek.rule.utils.Utils;
import org.springframework.context.annotation.Import;

import java.util.Map;

@Import(cn.hutool.extra.spring.SpringUtil.class)

/**
 * @Author weibi
 * @Date 2020/3/3 10:31
 * @Version 1.0
 * @Description TODO
 **/
public class ValueEngineStrategy {

    public static ValueEngineStrategy getInstance() {
        return ValueEngineHandle.instance;
    }

    private ValueEngineStrategy() {
    }

    private static class ValueEngineHandle {
        private static ValueEngineStrategy instance = new ValueEngineStrategy();
    }

    public Object getValue(Map<String, Object> map,  // 请求参数map
                           Object object, // 请求入参obj
                           String value  // 入参规则值
    ) {

        try {
//            value = Utils.getRuleStr(value);
            String[] prefixArr = value.split("\\#");
            ValueEnums enums = ValueEnums.getValueEnums(prefixArr[0]);
            IValueEngine iValueEngine = (IValueEngine) SpringUtil.getBean(enums.getClazz());
            return iValueEngine.process(map, object, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
