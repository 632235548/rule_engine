package com.iflytek.rule.strategy.resulttype;

import com.iflytek.rule.model.RuleResultModel;

public interface IResultType {

    Object resultOperator(Object requestObj,
                          String res,
                          String resObjPath,
                          String method,
                          RuleResultModel resultModel);

}
