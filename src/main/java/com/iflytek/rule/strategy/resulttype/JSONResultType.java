package com.iflytek.rule.strategy.resulttype;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.strategy.ResultDataStrategy;
import com.iflytek.rule.utils.Utils;

/**
 * @Author weibi
 * @Date 2020/3/4 16:56
 * @Version 1.0
 * @Description TODO
 **/
public class JSONResultType implements IResultType {
    @Override
    public Object resultOperator(Object requestObj,
                                 String res,
                                 String resObjPath,
                                 String method,
                                 RuleResultModel resultModel) {

        JSONObject object = JSONUtil.parseObj(res);
        Object obj = Utils.getResult(resultModel, object);
        if (ObjectUtil.isEmpty(obj)) {
            return null;
        }

        JSONObject json = JSONUtil.parseObj(obj);
        if (json.containsKey("merchantErrMsg")) {
            return json;
        }

        return ResultDataStrategy.getInstance().
                createResult(requestObj,
                        obj,
                        resObjPath,
                        method,
                        resultModel);
    }


}
