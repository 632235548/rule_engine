package com.iflytek.rule.strategy.servertype;

import com.iflytek.rule.model.RuleMerchantModel;
import com.iflytek.rule.model.RuleRequestModel;

/**
 *
 */
public interface IServerType {

    Object operatorByServerType(
            RuleMerchantModel model,
            RuleRequestModel requestModel,
            String requestParam);

//    Object rpc();

}
