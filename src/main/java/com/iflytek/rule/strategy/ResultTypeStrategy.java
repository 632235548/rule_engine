package com.iflytek.rule.strategy;

import cn.hutool.extra.spring.SpringUtil;
import com.iflytek.rule.enums.ResultTypeEnums;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.strategy.resulttype.IResultType;
import org.springframework.context.annotation.Import;

@Import(SpringUtil.class)
/**
 * @Author weibi
 * @Date 2020/3/3 9:01
 * @Version 1.0
 * @Description 请求参数类型模型
 **/
public class ResultTypeStrategy {

    public static ResultTypeStrategy getInstance() {
        return ResultTypeHandle.instance;
    }

    private ResultTypeStrategy() {
    }

    private static class ResultTypeHandle {
        private static ResultTypeStrategy instance = new ResultTypeStrategy();
    }

    public Object result(String type,  // 类型 json 还是xml
                         Object requestObj, // 入参对象 , 结果值有些需要入参值填充
                         String res,   // 接口返回值
                         String method, // 讯飞对应的接口方法，后续做方法逻辑处理
                         String resObjPath,  // 返回对象
                         RuleResultModel resultModel
    ) {

        try {
            ResultTypeEnums enums = ResultTypeEnums.getResultTypeEnums(type);
            IResultType iResultType = (IResultType) SpringUtil.getBean(enums.getClazz());
            return iResultType.resultOperator(requestObj,res, resObjPath, method, resultModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
