package com.iflytek.rule.strategy.valueengine;

import cn.hutool.core.util.ReflectUtil;
import com.iflytek.rule.utils.Utils;

import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/3 17:10
 * @Version 1.0
 * @Description 格式：java#类路径#方法#参数#参数
 **/
public class JavaValueEngine implements IValueEngine {
    @Override
    public Object process(Map<String, Object> map,
                          Object object,
                          String value) {
        try {
            String[] arr = value.split("\\#");
            Class clazz = Class.forName(arr[1]);
            if (arr.length > 3) {
                String[] strArr = new String[arr.length - 3];
                for (int i = 3; i < arr.length; i++) {
                    String st = Utils.toStr(arr[i], map,object);
                    strArr[i - 3] = st;
                }
                return ReflectUtil.invoke(clazz.newInstance(), arr[2], strArr);
            }
            return ReflectUtil.invoke(clazz.newInstance(), arr[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
