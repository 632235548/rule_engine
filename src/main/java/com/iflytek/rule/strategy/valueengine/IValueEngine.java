package com.iflytek.rule.strategy.valueengine;

import java.util.Map;

public interface IValueEngine {

    Object process(Map<String, Object> map,
                   Object object,
                   String value);

}
