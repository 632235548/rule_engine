package com.iflytek.rule.strategy.valueengine;

import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/4 8:17
 * @Version 1.0
 * @Description 固定值没有前缀不做任何处理，格式：固定值
 **/
public class FixValueEngine implements IValueEngine {
    @Override
    public Object process(Map<String, Object> map, Object object, String value) {
        return value;
    }
}
