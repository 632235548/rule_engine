package com.iflytek.rule.strategy.valueengine;

import cn.hutool.core.util.ReflectUtil;

import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/4 8:17
 * @Version 1.0
 * @Description 格式：attr#属性：接口入参映射
 **/
public class AttrValueEngine implements IValueEngine {

    @Override
    public Object process(Map<String, Object> map,Object object, String value) {
        // 反射获取属性值
        String attrName = value.split("\\#")[1];
        return ReflectUtil.getFieldValue(object, attrName);
    }
}
