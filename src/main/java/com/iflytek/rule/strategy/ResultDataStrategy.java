package com.iflytek.rule.strategy;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.iflytek.rule.enums.ResultDataEnums;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.strategy.resultdata.IResultData;
import org.springframework.context.annotation.Import;

@Import(SpringUtil.class)
/**
 * @Author weibi
 * @Date 2020/3/3 9:01
 * @Version 1.0
 * @Description 请求参数类型模型
 **/
public class ResultDataStrategy {

    public static ResultDataStrategy getInstance() {
        return ResultDataHandle.instance;
    }

    private ResultDataStrategy() {
    }

    private static class ResultDataHandle {
        private static ResultDataStrategy instance = new ResultDataStrategy();
    }

    public Object createResult(Object requestObj,
                               Object res,
                               String resObPath,
                               String methodType,
                               RuleResultModel resultModel) {

        try {
            ResultDataEnums enums = ResultDataEnums.getResultDataEnums(methodType);
            IResultData iResultData = (IResultData) SpringUtil.getBean(enums.getClazz());
            return iResultData.result(requestObj, res, resObPath, resultModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
