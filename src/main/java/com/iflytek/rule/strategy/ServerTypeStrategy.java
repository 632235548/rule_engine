package com.iflytek.rule.strategy;

import cn.hutool.extra.spring.SpringUtil;
import com.iflytek.rule.enums.ServerTypeEnums;
import com.iflytek.rule.model.RuleMerchantModel;
import com.iflytek.rule.model.RuleRequestModel;
import com.iflytek.rule.strategy.servertype.IServerType;
import org.springframework.context.annotation.Import;

@Import(SpringUtil.class)
/**
 * @Author weibi
 * @Date 2020/3/3 9:01
 * @Version 1.0
 * @Description 请求参数类型模型
 **/
public class ServerTypeStrategy {

    public static ServerTypeStrategy getInstance() {
        return ServerTypeHandle.instance;
    }

    private ServerTypeStrategy() {
    }

    private static class ServerTypeHandle {
        private static ServerTypeStrategy instance = new ServerTypeStrategy();
    }

    public Object rpc(RuleMerchantModel model,
                      RuleRequestModel requestModel,
                      String requestParam) {

        try {
            ServerTypeEnums enums = ServerTypeEnums.getServerTypeEnums(model.getServerType());
            IServerType iServerType = (IServerType) SpringUtil.getBean(enums.getClazz());
            return iServerType.operatorByServerType(model, requestModel, requestParam);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
