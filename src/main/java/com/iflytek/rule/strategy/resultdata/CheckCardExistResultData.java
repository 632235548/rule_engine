package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONObject;
import com.iflytek.rule.model.RuleResultModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author weibi
 * @Date 2020/3/26 9:04
 * @Version 1.0
 * @Description TODO
 **/
public class CheckCardExistResultData implements IResultData {


    @Autowired
    private GetPatinfoReultData getPatinfoReultData;

    @Override
    public Object result(Object requestObj,
                         Object res,
                         String resObjPath,
                         RuleResultModel resultModel) {

        try {
            List list = (List) getPatinfoReultData.result(requestObj, res, resObjPath, resultModel);
            JSONObject resObj = new JSONObject();
            if (CollectionUtil.isEmpty(list)) {
                resObj.put("isExist", "1");
            } else {
                resObj.put("isExist", "2");
            }
            return resObj;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
