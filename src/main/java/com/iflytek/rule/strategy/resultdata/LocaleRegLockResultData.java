package com.iflytek.rule.strategy.resultdata;

import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;

/**
 * @Author weibi
 * @Date 2020/3/13 13:16
 * @Version 1.0
 * @Description 当日锁号
 **/
public class LocaleRegLockResultData implements IResultData {

    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {
        return Utils.rpcResult(res, resObjPath, resultModel);
    }
}
