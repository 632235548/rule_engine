package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.enums.CodeEnums;
import com.iflytek.rule.exceptions.BusinessException;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/10 16:28
 * @Version 1.0
 * @Description TODO
 **/
public class GetOrderRegInfoforDiagnoseResultData implements IResultData {
    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {

        List list = (List) Utils.rpcResult(res, resObjPath, resultModel);

        if (CollectionUtil.isEmpty(list)) {
            throw new BusinessException(CodeEnums.RESULT_DATA_ERROR);
        }
        List<Map<String, Object>> resultList = new ArrayList<>();

        list.forEach(item -> {
            JSONObject obj = JSONUtil.parseObj(item);
            String regNumInfo = getRegNumInfo(obj);
            Map<String, Object> map = new HashMap<>(5);
            map.put("doctorId", obj.getStr("doctorId"));
            map.put("docName", obj.getStr("docName"));
            map.put("deptId", ReflectUtil.getFieldValue(requestObj, "deptId"));
            map.put("deptName", obj.getStr("deptName"));
            map.put("regNumInfo", regNumInfo);

            resultList.add(map);
        });
        return resultList;
    }


    private String getRegNumInfo(JSONObject obj) {

        try {
            // 获取当天的时间
            Date currentDay = Utils.getCurrentDate("yyyy-MM-dd");
            // 获取接口中的时间
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String dateStr = obj.getStr("regDate");
            Date date = format.parse(dateStr);

            // 获取号源
            String regRemained = obj.getStr("regLeaveCount");
            int regRemainedNum = StringUtils.isEmpty(regRemained) ? 0 : Integer.parseInt(regRemained);


            if (currentDay.equals(date)) {
                // 当天的时间
                return 0 == regRemainedNum ? RegNumEnums.ALL_NONE.getCode() + "" : RegNumEnums.CURRENT_ALL.getCode() + "";
            } else {
                // 之后的时间
                return 0 == regRemainedNum ? RegNumEnums.ALL_NONE.getCode() + "" : RegNumEnums.FUTURE_ALL.getCode() + "";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return RegNumEnums.ALL_NONE.getCode() + "";
    }

    enum RegNumEnums {

        // 1 全部无号 2 今日有号 3 预约有号

        ALL_NONE(1, "全部无号"),
        CURRENT_ALL(2, "今日有号"),
        FUTURE_ALL(3, "预约有号");

        private int code;
        private String msg;

        RegNumEnums(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
