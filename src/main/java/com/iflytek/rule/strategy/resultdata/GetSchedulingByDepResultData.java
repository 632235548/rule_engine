package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.enums.CodeEnums;
import com.iflytek.rule.exceptions.BusinessException;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.SchedulingUtils;
import com.iflytek.rule.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/10 14:30
 * @Version 1.0
 * @Description TODO
 **/
public class GetSchedulingByDepResultData implements IResultData {
    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {
        List<Object> rpcList = (List<Object>) Utils.rpcResult(res, resObjPath, resultModel);
        Map<String, Object> resultMap = new HashMap<>(3);
        if (null == rpcList) {
            throw new BusinessException(CodeEnums.RESULT_DATA_ERROR);
        }

        List<Map<String, String>> morList = new ArrayList<>();
        List<Map<String, String>> aftList = new ArrayList<>();
        rpcList.forEach(item -> {
            JSONObject obj = JSONUtil.parseObj(item);
            obj.put("deptId", ReflectUtil.getFieldValue(requestObj, "deptId"));
            Map<String, String> morningMap = SchedulingUtils.buildMorning(obj);
            if (!morningMap.isEmpty()) {
                morList.add(morningMap);
            }
            Map<String, String> afterMap = SchedulingUtils.buildAfternoon(obj);
            if (!afterMap.isEmpty()) {
                aftList.add(afterMap);
            }
        });

        List<List<Map<String, String>>> morResList = buildList(morList, ReflectUtil.getFieldValue(requestObj, "beginDate"), ReflectUtil.getFieldValue(requestObj, "endDate"));

        List<List<Map<String, String>>> aftResList = buildList(aftList, ReflectUtil.getFieldValue(requestObj, "beginDate"), ReflectUtil.getFieldValue(requestObj, "endDate"));

        if (CollectionUtil.isEmpty(morList) && CollectionUtil.isEmpty(aftResList)) {
            return resultMap;
        }

        resultMap.put("timeList", bulidTimeList(requestObj));
        resultMap.put("morList", morResList);
        resultMap.put("aftList", aftResList);
        return resultMap;
    }


    private List<List<Map<String, String>>> buildList(List<Map<String, String>> list, Object beginDate, Object endDate) {

        List<List<Map<String, String>>> result = new ArrayList<>();
        List<String> times = Utils.getDateRegion(String.valueOf(beginDate), String.valueOf(endDate));
        for (String time : times) {
            List<Map<String, String>> tmpList = new ArrayList<>();
            for (Map<String, String> map : list) {
                String regDate = map.get("regDate");
                if (time.equals(regDate)) {
                    tmpList.add(map);
                }
            }
            result.add(tmpList);
        }
        return result;
    }

    private List<Map<String, String>> bulidTimeList(Object reqObj) {
        List<String> dateList = Utils.getDateRegion(String.valueOf(ReflectUtil.getFieldValue(reqObj, "beginDate")), String.valueOf(ReflectUtil.getFieldValue(reqObj, "endDate")));
        List<Map<String, String>> list = new ArrayList<>(dateList.size());
        for (String date : dateList) {
            Map<String, String> map = new HashMap<>(2);
            map.put("regDate", date);
            map.put("regWeek", Utils.dateToWeek(date));

            list.add(map);
        }
        return list;
    }
}
