package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.enums.CodeEnums;
import com.iflytek.rule.exceptions.BusinessException;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.SchedulingUtils;
import com.iflytek.rule.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @Author weibi
 * @Date 2020/3/9 11:26
 * @Version 1.0
 * @Description TODO
 **/
public class GetSchedulingByDocResultData implements IResultData {
    @Override
    public Object result(Object requestObj,
                         Object res,
                         String resObjPath,
                         RuleResultModel resultModel) {

        List<Object> rpcList = (List<Object>) Utils.rpcResult(res, resObjPath, resultModel);

        List<Map<String, Object>> resultList = new ArrayList<>();
        if (CollectionUtil.isEmpty(rpcList)) {
            throw new BusinessException(CodeEnums.RESULT_DATA_ERROR);
        }

        Map<String, Object> tmpMap = new HashMap<>();
        rpcList.forEach(item -> {
            JSONObject obj = JSONUtil.parseObj(item);
            String date = obj.getStr("regDate");
            obj.put("deptId", ReflectUtil.getFieldValue(requestObj, "deptId"));

            List<Map<String, Object>> list = null;

            if (Objects.isNull(tmpMap.get(date))) {
                list = new ArrayList<>();
            } else {
                list = (List<Map<String, Object>>) tmpMap.get(date);
            }

            Map<String, String> morningMap = SchedulingUtils.buildMorning(obj);
            if (!morningMap.isEmpty()) {
                Map<String, Object> map1 = new HashMap<>(2);
                map1.put("regDate", date);
                map1.put("morning", morningMap);
                list.add(map1);
            }
            Map<String, String> afterMap = SchedulingUtils.buildAfternoon(obj);
            if (!afterMap.isEmpty()) {
                Map<String, Object> map1 = new HashMap<>(2);
                map1.put("regDate", date);
                map1.put("afternoon", afterMap);
                list.add(map1);
            }
            tmpMap.put(date, list);

        });

        List<String> times = Utils.getDateRegion(
                (String) ReflectUtil.getFieldValue(requestObj, "beginDate"),
                (String) ReflectUtil.getFieldValue(requestObj, "endDate"));
        for (String time : times) {
            Map<String, Object> map2 = new HashMap<>(4);
            map2.put("regDate", time);
            map2.put("regWeek", Utils.dateToWeek(time));
            List<Map<String, Object>> list = (List<Map<String, Object>>) tmpMap.get(time);
            if (CollectionUtil.isEmpty(list)) {
                map2.put("morning", "");
                map2.put("afternoon", "");
                resultList.add(map2);
                continue;
            }
            for (Map<String, Object> map : list) {
                if (map.containsKey("morning")) {
                    map2.put("morning", map.get("morning"));
                }
                if (map.containsKey("afternoon")) {
                    map2.put("afternoon", map.get("afternoon"));
                }
            }

            if (Objects.isNull(map2.get("morning"))) {
                map2.put("morning", "");
            }
            if (Objects.isNull(map2.get("afternoon"))) {
                map2.put("afternoon", "");
            }

            resultList.add(map2);
        }

        // 如果 morning， afternoon 都为空，则返回空对象，有一个则全部返回
        boolean flag = true;
        for (Map<String, Object> map : resultList) {

            if (Objects.nonNull(map.get("morning")) || Objects.nonNull(map.get("afternoon"))) {
                flag = false;
                break;
            }
        }
        if (flag) {
            return new ArrayList<>();
        }
        return resultList;
    }


}
