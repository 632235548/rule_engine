package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.cache.RedisConfig;
import com.iflytek.rule.cache.RedisUtils;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author weibi
 * @Date 2020/3/9 11:11
 * @Version 1.0
 * @Description TODO
 **/
public class GetDoctorInfoResultData implements IResultData {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private RedisConfig redisConfig;

    @Override
    public Object result(
            Object requestObj,
            Object res,
            String resObjPath,
            RuleResultModel resultModel) {

        List list = (List) Utils.rpcResult(res, resObjPath, resultModel);
        // 某科室下的医生放入缓存
        if (CollectionUtil.isEmpty(list)) {
            return list;
        }
        String key = Utils.setRedisKey(
                resultModel.getMerchant(),
                "getDoctorInfo",
                "docs"
        );
        redisUtils.hset(key, String.valueOf(ReflectUtil.getFieldValue(requestObj, "deptId")), list, redisConfig.getTimeout().get("docs"));
        return list;
    }
}
