package com.iflytek.rule.strategy.resultdata;

import com.iflytek.rule.model.RuleResultModel;

/**
 * @Author weibi
 * @Date 2020/3/4 19:51
 * @Version 1.0
 * @Description TODO
 **/
public interface IResultData {

    /**
     * @param res        请求HIS接口返回的对象
     * @param resObjPath 讯飞接口返回的对象路径
     * @return
     */
    Object result(Object requestObj,
                  Object res,
                  String resObjPath,
                  RuleResultModel resultModel);
}
