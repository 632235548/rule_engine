package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.cache.RedisConfig;
import com.iflytek.rule.cache.RedisUtils;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author weibi
 * @Date 2020/3/11 9:01
 * @Version 1.0
 * @Description TODO
 **/
public class GetPatinfoReultData implements IResultData {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private RedisConfig redisConfig;

    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {

        List list = (List) Utils.rpcResult(res, resObjPath, resultModel);
        if (!CollectionUtil.isEmpty(list)) {
            Object obj = list.get(0);
            JSONObject json = JSONUtil.parseObj(obj);
            // 数据入缓存
            String key = Utils.setRedisKey(resultModel.getMerchant(), "pat", json.getStr("patId"));
            redisUtils.set(key, obj, redisConfig.getTimeout().get("pat"));
        }
        return list;
    }
}
