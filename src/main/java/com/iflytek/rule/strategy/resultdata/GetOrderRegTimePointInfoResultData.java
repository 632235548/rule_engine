package com.iflytek.rule.strategy.resultdata;

import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;

/**
 * @Author weibi
 * @Date 2020/3/26 10:02
 * @Version 1.0
 * @Description TODO
 **/
public class GetOrderRegTimePointInfoResultData implements IResultData {
    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {
        return Utils.rpcResult(res, resObjPath, resultModel);
    }
}
