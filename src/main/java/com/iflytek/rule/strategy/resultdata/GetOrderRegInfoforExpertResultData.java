package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.enums.CodeEnums;
import com.iflytek.rule.exceptions.BusinessException;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/10 16:28
 * @Version 1.0
 * @Description TODO
 **/
public class GetOrderRegInfoforExpertResultData implements IResultData {
    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {
        List list = (List) Utils.rpcResult(res, resObjPath, resultModel);
        if (CollectionUtil.isEmpty(list)) {
            throw new BusinessException(CodeEnums.RESULT_DATA_ERROR);
        }

        Map<String, Integer> regNumMap = new HashMap<>(14);
        list.forEach(item -> {
            JSONObject obj = JSONUtil.parseObj(item);
            String date = obj.getStr("regDate");
            Integer RegRemainedNum = Integer.parseInt(obj.getStr("regLeaveCount"));
            regNumMap.put(date, (null == regNumMap.get(date) ? RegRemainedNum : (regNumMap.get(date) + RegRemainedNum)));

        });

        List<Map<String, Object>> resultList = new ArrayList<>();
        List<String> times = Utils.getDateRegion(
                String.valueOf(ReflectUtil.getFieldValue(requestObj, "beginDate")),
                String.valueOf(ReflectUtil.getFieldValue(requestObj, "endDate")));

        for (String time : times) {

            Map<String, Object> map = new HashMap<>(4);
            map.put("regDate", time);
            map.put("regNumInfo", (null == regNumMap.get(time) || 0 == regNumMap.get(time)) ? "0" : "1");
            map.put("deptId", String.valueOf(ReflectUtil.getFieldValue(requestObj, "deptId")));
            map.put("deptName", String.valueOf(ReflectUtil.getFieldValue(requestObj, "deptName")));

            resultList.add(map);
        }

        return resultList;
    }
}
