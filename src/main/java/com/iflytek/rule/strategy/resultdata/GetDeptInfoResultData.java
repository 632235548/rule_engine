package com.iflytek.rule.strategy.resultdata;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.iflytek.rule.cache.RedisConfig;
import com.iflytek.rule.cache.RedisUtils;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author weibi
 * @Date 2020/3/4 19:54
 * @Version 1.0
 * @Description 适合科室列表和医生列表接口
 **/
public class GetDeptInfoResultData implements IResultData {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private RedisConfig redisConfig;

    @Override
    public Object result(Object requestObj,
                         Object res,
                         String resObjPath,
                         RuleResultModel resultModel) {

        List list = (List) Utils.rpcResult(res, resObjPath, resultModel);

        if (CollectionUtil.isEmpty(list)) {
            return null;
        }

        String key = Utils.setRedisKey(
                resultModel.getMerchant(),
                "getDeptInfo",
                "depts"
        );
        String deptId = String.valueOf(ReflectUtil.getFieldValue(requestObj, "deptId"));
        if (StrUtil.isBlank(deptId)) {
            redisUtils.lSet(key, list, redisConfig.getTimeout().get("depts"));
        } else {
            // 说明有上一级
            redisUtils.hset(key, deptId, list, redisConfig.getTimeout().get("depts"));
        }

        return list;
    }

}
