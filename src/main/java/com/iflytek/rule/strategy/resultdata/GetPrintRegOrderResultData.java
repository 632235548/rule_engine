package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONObject;
import com.iflytek.rule.cache.RedisUtils;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author weibi
 * @Date 2020/3/11 13:35
 * @Version 1.0
 * @Description TODO
 **/
public class GetPrintRegOrderResultData implements IResultData {

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {
        // 所有数据将从缓存获取

        String patId = String.valueOf(ReflectUtil.getFieldValue(requestObj, "patId"));
        String deptId = String.valueOf(ReflectUtil.getFieldValue(requestObj, "deptId"));
        String doctId = String.valueOf(ReflectUtil.getFieldValue(requestObj, "doctorId"));
        // 科室信息
        String deptKey = Utils.setRedisKey(resultModel.getMerchant(), "dept", deptId);
        JSONObject deptInfo = (JSONObject) redisUtils.get(deptKey);
        // 医生信息
        String docKey = Utils.setRedisKey(resultModel.getMerchant(), "doc", doctId);
        JSONObject doctInfo = (JSONObject) redisUtils.get(docKey);
        // 用户信息
        String userKey = Utils.setRedisKey(resultModel.getMerchant(), "pat", patId);
        JSONObject userInfo = (JSONObject) redisUtils.get(userKey);
        // 挂号信息
        String regKey = Utils.setRedisKey(resultModel.getMerchant(), "reg", patId);
        JSONObject regInfo = (JSONObject) redisUtils.get(regKey);

        JSONObject jsonRes = Utils.combineJson(deptInfo, doctInfo, userInfo, regInfo);

        // 删除用户挂号信息
        redisUtils.del(userKey, regKey);

        return jsonRes;

    }
}
