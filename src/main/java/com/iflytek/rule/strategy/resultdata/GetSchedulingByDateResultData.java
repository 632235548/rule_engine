package com.iflytek.rule.strategy.resultdata;

import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;

/**
 * @Author weibi
 * @Date 2020/3/8 18:50
 * @Version 1.0
 * @Description 获取排班信息 by date
 **/
public class GetSchedulingByDateResultData implements IResultData {

    @Override
    public Object result(
            Object requestObj,
            Object res,
            String resObjPath,
            RuleResultModel resultModel) {
        return Utils.rpcResult(res, resObjPath, resultModel);
    }
}
