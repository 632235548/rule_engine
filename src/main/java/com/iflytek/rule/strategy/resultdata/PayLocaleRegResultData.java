package com.iflytek.rule.strategy.resultdata;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.iflytek.rule.cache.RedisUtils;
import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author weibi
 * @Date 2020/3/11 13:44
 * @Version 1.0
 * @Description TODO
 **/
public class PayLocaleRegResultData implements IResultData {

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {
        return Utils.rpcResult(res, resObjPath, resultModel);
    }
}
