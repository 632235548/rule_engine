package com.iflytek.rule.strategy.resultdata;

import com.iflytek.rule.model.RuleResultModel;
import com.iflytek.rule.utils.Utils;


/**
 * @Author weibi
 * @Date 2020/3/10 16:29
 * @Version 1.0
 * @Description TODO
 **/
public class GetAllRegOrderResultData implements IResultData {
    @Override
    public Object result(Object requestObj, Object res, String resObjPath, RuleResultModel resultModel) {
        return Utils.rpcResult(res, resObjPath, resultModel);
    }
}
