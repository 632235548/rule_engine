package com.iflytek.rule.dao;

import com.iflytek.rule.model.RuleMerchantModel;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

public interface RuleMerchantDao extends BaseMapper<RuleMerchantModel> {

    RuleMerchantModel findByMerchant(@Param("name") String merchant);

}
