package com.iflytek.rule.dao;

import com.iflytek.rule.model.RuleRequestModel;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

/**
 * @Author weibi
 * @Date 2020/3/2 17:20
 * @Version 1.0
 * @Description TODO
 **/
public interface RuleRequestDao extends BaseMapper<RuleRequestModel> {

    List<RuleRequestModel> findByParam(@Param("name") String merchant,
                                       @Param("method") String xfRequestMethod);

    List<RuleRequestModel> findByPid(@Param("pid") Long pid);

}
