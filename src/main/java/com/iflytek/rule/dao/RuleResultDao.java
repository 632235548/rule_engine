package com.iflytek.rule.dao;

import com.iflytek.rule.model.RuleResultModel;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

public interface RuleResultDao extends BaseMapper<RuleResultModel> {

    RuleResultModel findByParam(@Param("name") String merchant,
                                @Param("method") String xfRequestMethod);

}
