package com.iflytek.rule.autoconfiguration;

import com.iflytek.rule.strategy.requestparamtype.IRequestType;
import com.iflytek.rule.strategy.requestparamtype.JSONRequestType;
import com.iflytek.rule.strategy.requestparamtype.XMLRequestType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author weibi
 * @Date 2020/3/2 17:11
 * @Version 1.0
 * @Description TODO
 **/
@Configuration
@ConditionalOnClass({JSONRequestType.class,
        XMLRequestType.class})
public class RequestTypeAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(JSONRequestType.class)
    public IRequestType JSONRequestType() {
        return new JSONRequestType();
    }

    @Bean
    @ConditionalOnMissingBean(XMLRequestType.class)
    public IRequestType XMLRequestType() {
        return new XMLRequestType();
    }
}
