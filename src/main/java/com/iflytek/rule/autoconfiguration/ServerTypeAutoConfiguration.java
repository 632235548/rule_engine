package com.iflytek.rule.autoconfiguration;

import com.iflytek.rule.strategy.servertype.HttpServerType;
import com.iflytek.rule.strategy.servertype.IServerType;
import com.iflytek.rule.strategy.servertype.WebServiceServerType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author weibi
 * @Date 2020/3/2 17:11
 * @Version 1.0
 * @Description 目前没用该类
 **/
@Configuration
@ConditionalOnClass({WebServiceServerType.class,
        HttpServerType.class})
public class ServerTypeAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(WebServiceServerType.class)
    public IServerType WebServiceServerType() {
        return new WebServiceServerType();
    }

    @Bean
    @ConditionalOnMissingBean(HttpServerType.class)
    public IServerType HttpServerType() {
        return new HttpServerType();
    }
}
