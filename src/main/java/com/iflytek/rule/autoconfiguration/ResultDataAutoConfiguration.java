package com.iflytek.rule.autoconfiguration;

import com.iflytek.rule.strategy.resultdata.CheckCardExistResultData;
import com.iflytek.rule.strategy.resultdata.CreatePatMedCardResultData;
import com.iflytek.rule.strategy.resultdata.GetAllRegOrderResultData;
import com.iflytek.rule.strategy.resultdata.GetDeptInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetDoctorInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetLocaleRegTimePointInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetOrderRegInfoforDiagnoseResultData;
import com.iflytek.rule.strategy.resultdata.GetOrderRegInfoforExpertResultData;
import com.iflytek.rule.strategy.resultdata.GetOrderRegTimePointInfoResultData;
import com.iflytek.rule.strategy.resultdata.GetPatinfoReultData;
import com.iflytek.rule.strategy.resultdata.GetSchedulingByDateResultData;
import com.iflytek.rule.strategy.resultdata.GetSchedulingByDepResultData;
import com.iflytek.rule.strategy.resultdata.GetSchedulingByDocResultData;
import com.iflytek.rule.strategy.resultdata.GetUpperDeptInfoResultData;
import com.iflytek.rule.strategy.resultdata.IResultData;
import com.iflytek.rule.strategy.resultdata.LocaleRegLockResultData;
import com.iflytek.rule.strategy.resultdata.OrderRegLockResultData;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author weibi
 * @Date 2020/3/2 17:11
 * @Version 1.0
 * @Description TODO
 **/
@Configuration
@ConditionalOnClass({
        GetUpperDeptInfoResultData.class,
        GetDeptInfoResultData.class,
        GetDoctorInfoResultData.class,
        GetSchedulingByDateResultData.class,
        GetSchedulingByDocResultData.class,
        GetSchedulingByDepResultData.class,
        GetOrderRegInfoforDiagnoseResultData.class,
        GetOrderRegInfoforExpertResultData.class,
        CreatePatMedCardResultData.class,
        GetPatinfoReultData.class,
        LocaleRegLockResultData.class,
        OrderRegLockResultData.class,
        CheckCardExistResultData.class,
        GetOrderRegTimePointInfoResultData.class,
        GetLocaleRegTimePointInfoResultData.class,
        GetAllRegOrderResultData.class

})
public class ResultDataAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(GetUpperDeptInfoResultData.class)
    public IResultData GetUpperDeptInfoResultData() {
        return new GetUpperDeptInfoResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetDeptInfoResultData.class)
    public IResultData GetDeptInfoResultData() {
        return new GetDeptInfoResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetDoctorInfoResultData.class)
    public IResultData GetDoctorInfoResultData() {
        return new GetDoctorInfoResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetSchedulingByDateResultData.class)
    public IResultData GetSchedulingByDate() {
        return new GetSchedulingByDateResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetSchedulingByDocResultData.class)
    public IResultData GetSchedulingByDocResultData() {
        return new GetSchedulingByDocResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetSchedulingByDepResultData.class)
    public IResultData GetSchedulingByDepResultData() {
        return new GetSchedulingByDepResultData();
    }


    @Bean
    @ConditionalOnMissingBean(GetOrderRegInfoforDiagnoseResultData.class)
    public IResultData GetOrderRegInfoforDiagnoseResultData() {
        return new GetOrderRegInfoforDiagnoseResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetOrderRegInfoforExpertResultData.class)
    public IResultData GetOrderRegInfoforExpertResultData() {
        return new GetOrderRegInfoforExpertResultData();
    }

    @Bean
    @ConditionalOnMissingBean(CreatePatMedCardResultData.class)
    public IResultData CreatePatMedCardResultData() {
        return new CreatePatMedCardResultData();
    }


    @Bean
    @ConditionalOnMissingBean(GetPatinfoReultData.class)
    public IResultData GetPatinfoReultData() {
        return new GetPatinfoReultData();
    }


    @Bean
    @ConditionalOnMissingBean(LocaleRegLockResultData.class)
    public IResultData LocaleRegLockResultData() {
        return new LocaleRegLockResultData();
    }


    @Bean
    @ConditionalOnMissingBean(OrderRegLockResultData.class)
    public IResultData OrderRegLockResultData() {
        return new OrderRegLockResultData();
    }

    @Bean
    @ConditionalOnMissingBean(CheckCardExistResultData.class)
    public IResultData CheckCardExistResultData() {
        return new CheckCardExistResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetOrderRegTimePointInfoResultData.class)
    public IResultData GetOrderRegTimePointInfoResultData() {
        return new GetOrderRegTimePointInfoResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetLocaleRegTimePointInfoResultData.class)
    public IResultData GetLocaleRegTimePointInfoResultData() {
        return new GetLocaleRegTimePointInfoResultData();
    }

    @Bean
    @ConditionalOnMissingBean(GetAllRegOrderResultData.class)
    public IResultData GetAllRegOrderResultData() {
        return new GetAllRegOrderResultData();
    }

}
