package com.iflytek.rule.autoconfiguration;

import com.iflytek.rule.service.IBusinessService;
import com.iflytek.rule.service.impl.BusinessServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author weibi
 * @Date 2020/3/2 17:11
 * @Version 1.0
 * @Description TODO
 **/
@Configuration
@ConditionalOnClass({BusinessServiceImpl.class})
public class BusinessAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(BusinessServiceImpl.class)
    public IBusinessService businessService() {
        return new BusinessServiceImpl();
    }
}
