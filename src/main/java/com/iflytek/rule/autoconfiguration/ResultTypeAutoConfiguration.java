package com.iflytek.rule.autoconfiguration;

import com.iflytek.rule.strategy.resulttype.IResultType;
import com.iflytek.rule.strategy.resulttype.JSONResultType;
import com.iflytek.rule.strategy.resulttype.XMLResultType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author weibi
 * @Date 2020/3/2 17:11
 * @Version 1.0
 * @Description TODO
 **/
@Configuration
@ConditionalOnClass({JSONResultType.class,
        XMLResultType.class})
public class ResultTypeAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(JSONResultType.class)
    public IResultType JSONResultType() {
        return new JSONResultType();
    }

    @Bean
    @ConditionalOnMissingBean(XMLResultType.class)
    public IResultType XMLResultType() {
        return new XMLResultType();
    }
}
