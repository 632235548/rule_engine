package com.iflytek.rule.autoconfiguration;

import com.iflytek.rule.service.IRequestService;
import com.iflytek.rule.service.impl.RequestServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author weibi
 * @Date 2020/3/2 17:11
 * @Version 1.0
 * @Description TODO
 **/
@Configuration
@ConditionalOnClass(RequestServiceImpl.class)
@ComponentScan(basePackages = {"cn.hutool.extra.spring"})
public class RequestAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(RequestServiceImpl.class)
    public IRequestService requestService() {
        return new RequestServiceImpl();
    }


}
