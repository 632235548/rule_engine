package com.iflytek.rule.autoconfiguration;

import com.iflytek.rule.strategy.valueengine.AttrValueEngine;
import com.iflytek.rule.strategy.valueengine.FixValueEngine;
import com.iflytek.rule.strategy.valueengine.IValueEngine;
import com.iflytek.rule.strategy.valueengine.JavaValueEngine;
import com.iflytek.rule.strategy.valueengine.JsValueEngine;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author weibi
 * @Date 2020/3/2 17:11
 * @Version 1.0
 * @Description TODO
 **/
@Configuration
@ConditionalOnClass({AttrValueEngine.class,
        FixValueEngine.class,
        JavaValueEngine.class,
        JsValueEngine.class})
public class ValueEngineAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(AttrValueEngine.class)
    public IValueEngine AttrValueEngine() {
        return new AttrValueEngine();
    }

    @Bean
    @ConditionalOnMissingBean(JavaValueEngine.class)
    public IValueEngine JavaValueEngine() {
        return new JavaValueEngine();
    }

    @Bean
    @ConditionalOnMissingBean(FixValueEngine.class)
    public IValueEngine FixValueEngine() {
        return new FixValueEngine();
    }

    @Bean
    @ConditionalOnMissingBean(JsValueEngine.class)
    public IValueEngine JsValueEngine() {
        return new JsValueEngine();
    }
}
