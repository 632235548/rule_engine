package com.iflytek.rule.exceptions;

import com.iflytek.rule.enums.CodeEnums;
import lombok.Data;

/**
 * @Author weibi
 * @Date 2020/3/3 8:26
 * @Version 1.0
 * @Description TODO
 **/
@Data
public class BusinessException extends RuntimeException {

    private String code;
    private String msg;

    public BusinessException() {
        super();
    }

    public BusinessException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(CodeEnums enums) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
    }
}
