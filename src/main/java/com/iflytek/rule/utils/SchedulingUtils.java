package com.iflytek.rule.utils;

import cn.hutool.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/10 14:36
 * @Version 1.0
 * @Description  业务代码
 **/
public class SchedulingUtils {

    /**
     * 获取上午的排班信息
     *
     * @param obj
     * @return
     */
    public static Map<String, String> buildMorning(JSONObject obj) {
        Map<String, String> map = new HashMap<>(12);
        if ("下午".equals(obj.getStr("regTimeName"))) {
            return map;
        }
        map = buildData(obj, true);
        return map;
    }

    /**
     * 获取下午的排班信息
     *
     * @param obj
     * @return
     */
    public static Map<String, String> buildAfternoon(JSONObject obj) {

        Map<String, String> map = new HashMap<>(12);
        if ("上午".equals(obj.getStr("regTimeName"))) {
            return map;
        }
        map = buildData(obj, false);
        return map;
    }

    private static Map<String, String> buildData(JSONObject obj, boolean flag) {
        Map<String, String> map = new HashMap<>(12);
        map.put("regDate", obj.getStr("regDate"));
        map.put("arrangeId", obj.getStr("arrangeId"));
        map.put("deptName", obj.getStr("deptName"));
        map.put("deptId", obj.getStr("deptId"));
        map.put("doctorId", obj.getStr("doctorId"));
        map.put("docName", obj.getStr("docName"));
//        map.put("titleId", obj.getStr("RankID"));
//        map.put("titleName", obj.getStr("RankName"));
        map.put("regTimeId", flag ? "1" : "2");  // 1：上午 2：下午 3:夜间
        map.put("regTimeName", obj.getStr("regTimeName"));
//        map.put("clin", obj.getStr("Clin"));
        map.put("regFee", obj.getStr("regFee"));
        map.put("regLeaveCount", obj.getStr("regLeaveCount"));
        map.put("regTotalCount", obj.getStr("regTotalCount"));
        map.put("timeRegion", obj.getStr("timeRegion"));
        return map;
    }
}
