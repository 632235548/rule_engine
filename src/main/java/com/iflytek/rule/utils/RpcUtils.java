package com.iflytek.rule.utils;

import cn.hutool.http.HttpUtil;

import java.util.Map;

/**
 * @Author weibi
 * @Date 2020/3/6 21:14
 * @Version 1.0
 * @Description TODO
 **/
public class RpcUtils {

    public String get(String url, Map<String, Object> params) {
        return HttpUtil.get(url, params);
    }

    public String post(String url, Map<String, Object> params) {
        return HttpUtil.post(url, params);
    }

}
