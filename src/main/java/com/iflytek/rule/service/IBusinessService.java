package com.iflytek.rule.service;

/**
 * @Author weibi
 * @Date 2020/3/3 10:39
 * @Version 1.0
 * @Description TODO
 **/
public interface IBusinessService {

    /**
     * 接口不传递，只单执行或者组合
     *
     * @param requestObj
     * @param resObjPath
     * @param hospitalEname
     * @param xfRequestMethod
     * @return
     */
    public Object operator(Object requestObj, // 入参对象
                           String resObjPath, // 出参对象
                           String hospitalEname,
                           String xfRequestMethod);

}
