package com.iflytek.rule.service;

import com.iflytek.rule.model.RuleRequestModel;

public interface IRequestService {

    Object requestParam(Object obj,
                        String hospitalEname,
                        String xfRequestMethod,
                        RuleRequestModel model);

}
