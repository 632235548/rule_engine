package com.iflytek.rule.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.iflytek.rule.enums.CodeEnums;
import com.iflytek.rule.exceptions.BusinessException;
import com.iflytek.rule.model.RuleRequestModel;
import com.iflytek.rule.service.IRequestService;
import com.iflytek.rule.strategy.RequestTypeStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @Author weibi
 * @Date 2020/3/2 17:08
 * @Version 1.0
 * @Description TODO
 **/
@Service("requestService")
public class RequestServiceImpl implements IRequestService {

    private final static Logger log = LoggerFactory.getLogger(RequestServiceImpl.class);

    /**
     * @param obj             #接口入参实例#
     * @param merchant
     * @param xfRequestMethod
     * @param model
     * @return
     */
    @Override
    public Object requestParam(Object obj,
                               String merchant,
                               String xfRequestMethod,
                               RuleRequestModel model) {


        if (ObjectUtil.isNull(model)) {
            log.error("request model is null,xfRequestMethod={}", xfRequestMethod);
            throw new BusinessException(CodeEnums.NULL_OBJECT);
        }

        String requestParam = model.getRequestParam();
        if (StrUtil.isEmptyIfStr(requestParam)) {
            log.error("requestParam model is null,xfRequestMethod={}", xfRequestMethod);
            throw new BusinessException(CodeEnums.NULL_PARAM);
        }

        Integer requestType = model.getRequestParamType();
        if (ObjectUtil.isEmpty(requestType)) {
            log.error("requestType is null,xfRequestMethod={}", xfRequestMethod);
            throw new BusinessException(CodeEnums.NULL_PARAM);
        }

        // 入参解析
        return RequestTypeStrategy.getInstance().requestType(obj, model);
    }
}
